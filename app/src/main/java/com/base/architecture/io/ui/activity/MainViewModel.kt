package com.base.architecture.io.ui.activity

import com.base.architecture.io.baseclasses.BaseViewModel
import com.base.architecture.io.events.SingleLiveEvent

class MainViewModel : BaseViewModel() {

    var themeEvent = SingleLiveEvent<Any>()

}