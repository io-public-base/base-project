package com.base.architecture.io.eventbus.messages

data class EventNavigateToViewPdf (val position: Int)