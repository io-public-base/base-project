package com.base.architecture.io.datalayer.local.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "PdfRecentFiles")
data class PdfStoreItems (

    @PrimaryKey
    val pdfUri: String,

    val pdfTitle: String,
    val pdfSize: String,
    val pdfAddedDate: String,
    val createdAt: String,
    val modifiedAt: String,
    val timeStamp: String
    )