package com.base.architecture.io.rawcollections.enumm

enum class ListActionEnums {
    IS_DELETED,
    IS_RENAMED,
    CLEAR_HISTORY_TRIGGERED
}