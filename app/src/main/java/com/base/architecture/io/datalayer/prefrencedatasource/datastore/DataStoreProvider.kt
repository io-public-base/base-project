package com.base.architecture.io.datalayer.prefrencedatasource.datastore

import android.content.Context
import androidx.datastore.preferences.*
import com.base.architecture.io.shared.AppConstants
import com.base.architecture.io.shared.PublicValue
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map


class DataStoreProvider(context: Context) {

    //Create the dataStore
    private val dataStore = context.createDataStore(name = AppConstants.DataStore.DATA_STORE_NAME)

    //Create some keys
    companion object {
        val CURRENT_THEME = preferencesKey<String>(AppConstants.DataStore.CURRENT_THEME)
        val SELECTED_APP_LANGUAGE =
            preferencesKey<String>(AppConstants.DataStore.SELECTED_APP_LANGUAGE)
        val SELECTED_PDF_SORTING_TITLE =
            preferencesKey<String>(AppConstants.DataStore.SELECTED_VIEW_PDF_SORTING_TITLE)
        val PAGE_BY_PAGE = preferencesKey<Boolean>(AppConstants.DataStore.PAGE_BY_PAGE)
    }

    suspend fun storeSelectedLanguage(lang: String) {
        dataStore.edit {
            it[SELECTED_APP_LANGUAGE] = lang
        }
    }

    suspend fun storePdfPageByPage(b: Boolean) {
        dataStore.edit {
            it[PAGE_BY_PAGE] = b
        }
    }

    val selectedThemeFlow: Flow<Any> = dataStore.data.map {
        it[CURRENT_THEME] ?: PublicValue.DEFAULT_THEME
    }

    val selectedAppLanguageFlow: Flow<Any> = dataStore.data.map {
        it[SELECTED_APP_LANGUAGE] ?: PublicValue.APP_DEFAULT_LANGUAGE
    }

    suspend fun clearPreferStoring() {
        dataStore.edit {
            it.remove(SELECTED_PDF_SORTING_TITLE)
        }
    }

}