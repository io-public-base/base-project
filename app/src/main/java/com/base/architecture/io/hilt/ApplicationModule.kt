package com.base.architecture.io.hilt

import android.content.Context
import com.base.architecture.io.datalayer.controller.DataLayer
import com.base.architecture.io.datalayer.local.database.AppDatabase
import com.base.architecture.io.datalayer.prefrencedatasource.datastore.DataStoreProvider
import com.base.architecture.io.datalayer.prefrencedatasource.sharedprefrence.Storage
import com.base.architecture.io.datalayer.repositories.ModuleOneRepo
import com.base.architecture.io.datalayer.repositories.ModuleSecondRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class ApplicationModule {

    @Singleton
    @Provides
    fun provideDataStoreProvider(@ApplicationContext appContext: Context) =
        DataStoreProvider(appContext)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideDataLayer(
        repositoryRemoteOne: ModuleOneRepo,
        repositoryRemoteSecond: ModuleSecondRepo,
    ) =
        DataLayer(repositoryRemoteOne,repositoryRemoteSecond)

    @Singleton
    @Provides
    fun provideDao(db: AppDatabase) = db.datasource()

    @Singleton
    @Provides
    fun provideRepository(db: AppDatabase) = db.datasource()


    @Singleton
    @Provides
    fun provideStorage(@ApplicationContext appContext: Context) =
        Storage(appContext)

}