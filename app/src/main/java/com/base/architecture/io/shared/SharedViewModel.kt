package com.base.architecture.io.shared

import androidx.lifecycle.MutableLiveData
import com.base.architecture.io.anchors.PdfPaths
import com.base.architecture.io.baseclasses.BaseViewModel

class SharedViewModel : BaseViewModel() {

    private val _pdfPath = MutableLiveData("")
    fun setPdfPath(path: String) {
        _pdfPath.value = path
    }
    fun getPdfPath() = _pdfPath.value


    private val _pdfDetails = MutableLiveData<PdfPaths>()
    fun setPdfDetailsModel(model: PdfPaths){
        _pdfDetails.value = model
    }
    fun getPdfDetailsModel(): PdfPaths = _pdfDetails.value!!

    private val _webURL = MutableLiveData("")
    fun setWebURL(url: String) {
        _webURL.value = url
    }
    fun getWebURL() = _webURL.value
}