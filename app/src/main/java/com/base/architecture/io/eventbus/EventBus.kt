package com.base.architecture.io.eventbus

import com.base.architecture.io.eventbus.messages.*

class EventBus : BaseEventBus() {

    companion object {
        val instance = EventBus()
    }

    /**
     * common
     */

    fun removeAllSticky() {
        removeAllStickyEvent()
    }


    /**
     * Base Fragment
     */
    fun onClickToolbarBack() {
        postEvent(EventClickedToolbarBack())
    }

    fun onClickNavDrawer() {
        postEvent(EventOpenNavDrawer())
    }

}