package com.base.architecture.io.manager

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Build
import com.base.architecture.io.ui.activity.MainActivity
import com.base.architecture.io.datalayer.prefrencedatasource.datastore.DataStoreProvider
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*


object LocaleManager {
    private lateinit var dataStoreProvider: DataStoreProvider
    @SuppressLint("DefaultLocale", "ObsoleteSdkInt")
    fun setAppLocale(context: Context, localeCode: String) {
        try {
            dataStoreProvider = DataStoreProvider(context)
            val resources = context.resources
            val dm = resources.displayMetrics
            val config = resources.configuration
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                config.setLocale(Locale(localeCode.toLowerCase())).let {
                    //persistAppLanguage(localeCode)
                }
            } else {
                config.locale = Locale(localeCode.toLowerCase())
                //persistAppLanguage(localeCode)
            }
            resources.updateConfiguration(config, dm)
            //restartApplication(context)
        } catch (ex: Exception) {
        }

    }

    fun persistAppLanguage(lan: String){
        GlobalScope.launch {
            dataStoreProvider.storeSelectedLanguage(lan)
        }
    }






     fun restartApplication(context: Context) {
        val intent = Intent(context, MainActivity::class.java)
        //intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK // I have tried almost every flag combo here!
         intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        context.startActivity(intent)
    }
}