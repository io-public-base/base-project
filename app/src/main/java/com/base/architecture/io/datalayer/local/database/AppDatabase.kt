package com.base.architecture.io.datalayer.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.base.architecture.io.datalayer.local.database.entity.PdfStoreItems
import com.base.architecture.io.datalayer.localdatasource.DataSource
import com.base.architecture.io.shared.AppConstants

@Database(entities = [PdfStoreItems::class], version = 2, exportSchema = false)
abstract class AppDatabase : RoomDatabase(){

    abstract fun datasource(): DataSource

    companion object {
        @Volatile private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
            instance
                ?: synchronized(this) { instance
                    ?: buildDatabase(
                        context
                    )
                        .also { instance = it } }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, AppDatabase::class.java, AppConstants.DbConfiguration.DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }
}

