package com.base.architecture.io.eventbus

import org.greenrobot.eventbus.EventBus
import timber.log.Timber

abstract class BaseEventBus {
    private val eventBus = EventBus.getDefault()

    fun subscribe(subscriber: Any) {
        if (eventBus.isRegistered(subscriber).not())
            eventBus.register(subscriber)
    }

    fun unsubscribe(subscriber: Any) {
        eventBus.unregister(subscriber)
    }

    protected fun postEvent(event: Any) {
        Timber.d(event.toString())
        eventBus.post(event)
    }

    protected fun postStickyEvent(event: Any) {
        Timber.d(event.toString())
        eventBus.postSticky(event)
    }

    protected fun <T> getStickyEvent(eventType: Class<T>): T {
        Timber.d(eventType.toString())
        return eventBus.getStickyEvent(eventType)
    }

    protected fun <T> getAndRemoveStickyEvent(eventType: Class<T>): T {
        Timber.d(eventType.toString())
        return eventBus.removeStickyEvent(eventType)
    }

    protected fun removeAllStickyEvent() {
        Timber.d("removeAllStickyEvent")
        eventBus.removeAllStickyEvents()
    }
}