package com.base.architecture.io.datalayer.localdatasource

import androidx.room.*

@Dao
interface DataSource {

    @Query("DELETE FROM PdfRecentFiles")
    suspend fun transit(str: String): String = str

    @Query("DELETE FROM PdfRecentFiles")
    fun dov(str: String): String = str
}