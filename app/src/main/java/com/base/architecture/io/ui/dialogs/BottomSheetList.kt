package com.base.architecture.io.ui.dialogs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.widget.FrameLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.base.architecture.io.databinding.LayoutBottomSheetListBinding
import com.base.architecture.io.datalayer.prefrencedatasource.sharedprefrence.Storage
import com.base.architecture.io.rawcollections.enumm.ViewPdfActions
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class BottomSheetList : BottomSheetDialogFragment() {

    private var binding: LayoutBottomSheetListBinding? = null

//    @Inject
//    lateinit var dataStoreProvider: DataStoreProvider

    @Inject
    lateinit var storage: Storage

    companion object {
        //arguments

        fun getInstance(
            callback: ((ViewPdfActions) -> Unit)
        ): BottomSheetList {
            return BottomSheetList()
        }


    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = LayoutBottomSheetListBinding.inflate(inflater)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try {
            view.viewTreeObserver.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    view.viewTreeObserver.removeOnGlobalLayoutListener(this)
                    val dialog = dialog as BottomSheetDialog
                    val bottomSheet =
                        dialog.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as FrameLayout?
                    val behavior = BottomSheetBehavior.from(bottomSheet!!)
                    behavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}