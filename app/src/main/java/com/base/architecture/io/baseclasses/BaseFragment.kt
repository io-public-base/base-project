package com.base.architecture.io.baseclasses

import android.Manifest
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavDirections
import androidx.navigation.fragment.findNavController
import com.base.architecture.io.datalayer.prefrencedatasource.datastore.DataStoreProvider
import com.base.architecture.io.datalayer.prefrencedatasource.sharedprefrence.Storage
import com.base.architecture.io.rawcollections.temp.Temp
import com.base.architecture.io.shared.PublicValue
import com.base.architecture.io.shared.SharedViewModel
import com.base.architecture.io.ui.dialogs.DialogClass
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*


abstract class BaseFragment<T : ViewDataBinding, V : BaseViewModel> : Fragment() {

    private var mActivity: BaseActivity<*, *>? = null
    lateinit var mViewDataBinding: T
    protected lateinit var mViewModel: V
    protected lateinit var loadingDialog: Dialog
    protected lateinit var temp: Temp
    protected lateinit var dataStoreProvider: DataStoreProvider
    protected lateinit var storage: Storage
    lateinit var sharedViewModel: SharedViewModel


    abstract val layoutId: Int
    abstract val viewModel: Class<V>
    abstract val bindingVariable: Int


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mViewDataBinding = DataBindingUtil.inflate(inflater, layoutId, container, false)
        return mViewDataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewDataBinding.setVariable(bindingVariable, mViewModel)
        mViewDataBinding.lifecycleOwner = this
        mViewDataBinding.executePendingBindings()
        temp = Temp()
        dataStoreProvider = DataStoreProvider(requireContext())
        storage = Storage(requireContext())
        loadingDialog = DialogClass.loadingDialog(requireContext())
        subscribeToShareLiveData()
        subscribeToNavigationLiveData()
        subscribeToViewLiveData()
        manipulateTheme()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(viewModel)
        sharedViewModel = ViewModelProviders.of(requireActivity()).get(SharedViewModel::class.java)
        subscribeToNetworkLiveData()
    }

    open fun subscribeToViewLiveData() {
        manageThemeSP()
    }

    /**
     * navigation
     */
    fun navigate(action: NavDirections) {
        findNavController().navigate(action)
    }

    private fun isDarkTheme(): Boolean {
        return this.resources.configuration.uiMode and
                Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
    }

    private fun isDarkThemeSP() : String{
        return storage.getPreferredTheme()
    }

    private fun manageThemeSP(){
        if (isDarkThemeSP().equals(PublicValue.DARK_THEME)){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }else if (isDarkThemeSP().equals(PublicValue.DEFAULT_THEME)){

        }
    }

    open fun manipulateTheme(){
        if(isDarkTheme()){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            requireActivity().window.decorView.systemUiVisibility =
                requireActivity().window.decorView.systemUiVisibility.and(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv()) or requireActivity().window.decorView.systemUiVisibility.and(
                    View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR.inv()
                )
            CoroutineScope(IO).launch {
                storage.setPreferredTheme(PublicValue.DARK_THEME)
            }
        }else
        {
            requireActivity().window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR or View.SYSTEM_UI_FLAG_LIGHT_NAVIGATION_BAR
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity<*, *>)
            this.mActivity = context
    }

    fun View.hideKeyboard() {
        try {
            val inputMethodManager =
                context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }


    fun isValidEmail(text: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(text).matches()
    }


    open fun subscribeToShareLiveData() {}

    open fun subscribeToNetworkLiveData() {}

    open fun subscribeToNavigationLiveData() {}



    fun checkPermission(permissions: Array<String>): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkPermissionsGrant(permissions)
        } else true
    }

    fun checkPermissionsGrant(requestedPermissions: Array<String>): Boolean {
        val notGrantedPermissions: MutableList<String> = ArrayList()
        for (requestedPermission in requestedPermissions) {
            if (ContextCompat.checkSelfPermission(requireContext(),requestedPermission) != PackageManager.PERMISSION_GRANTED) {
                notGrantedPermissions.add(requestedPermission)
            }
        }
        if (notGrantedPermissions.size > 0) {
            requestNotGrantedPermissions(notGrantedPermissions)
            return false
        }
        return true
    }

    fun requestNotGrantedPermissions(notGrantedPermissions: List<String>) {
        requestPermission()
        requestPermissions(
            notGrantedPermissions.toTypedArray(),
            PublicValue.KEY_REQUEST_PERMISSIONS
        )
    }

    open fun launchPermissionsHandler() {
        val needPermissions = arrayOf<String>(
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if (Environment.isExternalStorageManager()) {
                //launchPicker()
            } else {
                requestPermission()
            }
        } else {
            if (!checkPermission(needPermissions)) {
                return
            } else {
                //launchPicker()
            }
        }
    }

    open fun launchSavingPermissions(){
        val needPermissions = arrayOf<Any>(
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        requestPermissions(
            arrayOf("android.permission.WRITE_EXTERNAL_STORAGE"),
            PublicValue.KEY_SAVING_REQUEST_PERMISSIONS
        )
    }

    fun launchPicker() {
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        intent.type = "application/pdf"
        try {
            startActivityForResult(intent, PublicValue.KEY_REQUEST_FILE_PICKER)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(requireContext(), "user that file manager not working", Toast.LENGTH_SHORT).show()
        }
    }

    protected open fun requestPermission(): Boolean? {
        return try {
            val intent = Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
            intent.addCategory("android.intent.category.DEFAULT")
            intent.data = Uri.parse(String.format("package:%s", requireActivity().packageName))
            startActivityForResult(intent, PublicValue.KEY_REQUEST_PERMISSIONS_11)
            true
        } catch (e: Exception) {
            Log.i("version", e.message!!)
            false
        }
    }

    open fun getLocalDate() : String{
        val sdf = SimpleDateFormat("dd/M/yyyy")
        val currentDate = sdf.format(Date())
        return currentDate
    }

    open fun getLocalTimeStamp() : String{
        val stamp = System.currentTimeMillis()
        return stamp.toString()
    }


    open fun showToast(message: String){
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    open fun showLongToast(message: String){
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }


}