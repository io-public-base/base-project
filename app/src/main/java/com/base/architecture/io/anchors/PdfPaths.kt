package com.base.architecture.io.anchors

data class PdfPaths(
    val absolutePathList: ArrayList<String>,
    val password: String,
    val fileName: String,
    val parentFile : String
)
