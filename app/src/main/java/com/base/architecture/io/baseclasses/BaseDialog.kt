package com.base.architecture.io.baseclasses

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.DialogFragment
import com.base.architecture.io.R
import com.base.architecture.io.shared.SharedViewModel
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

abstract class BaseDialog <T : ViewDataBinding> : DialogFragment(){
    lateinit var binding: T
    abstract val resLayout: Int
    lateinit var sharedViewModel: SharedViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedViewModel = SharedViewModel()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        return dialog
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.d("onCreateView")
        binding = DataBindingUtil.inflate(inflater, resLayout, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("onViewCreated")
    }

    override fun onResume() {
        super.onResume()
        Timber.d("onResume")
        val layoutParams = dialog?.window?.attributes
        val width = resources.getDimensionPixelSize(R.dimen.dimen_default_dialog)
        layoutParams?.let {
            it.width = width
            it.height = WindowManager.LayoutParams.WRAP_CONTENT
        }
        dialog?.window?.let {
            it.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            it.attributes = layoutParams ?: layoutParams
        }
    }

    override fun onCancel(dialog: DialogInterface) {
        super.onCancel(dialog)
        Timber.d("onCancel")
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        Timber.d("onDismiss")
    }

    fun setTopAnimation() {
        dialog?.window?.setWindowAnimations(R.style.StyleDialogAnimTop)
    }

    open fun getLocalDate() : String{
        val sdf = SimpleDateFormat("dd/M/yyyy")
        val currentDate = sdf.format(Date())
        return currentDate
    }

    open fun getLocalTimeStamp() : String{
        val stamp = System.currentTimeMillis()
        return stamp.toString()
    }
}