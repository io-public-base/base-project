package com.base.architecture.io.ui.fragments.demo

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.base.architecture.io.baseclasses.BaseViewModel
import com.base.architecture.io.datalayer.controller.DataLayer
import com.base.architecture.io.events.SingleLiveEvent
import kotlinx.coroutines.launch


class DemoViewModel @ViewModelInject constructor(
    private var dataLayer: DataLayer
) : BaseViewModel(){

    fun addDov(argument: String): SingleLiveEvent<String>{
        val value = SingleLiveEvent<String>()
        viewModelScope.launch{
            val dataResistance = dataLayer.makeDov(argument)
            value.postValue(dataResistance)
        }
        return value
    }
}