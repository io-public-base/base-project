package com.base.architecture.io.shared

import androidx.annotation.StringDef

object AppConstants {
    @StringDef(DataStore.DATA_STORE_NAME,DataStore.CURRENT_THEME,DataStore.SELECTED_APP_LANGUAGE, DataStore.SELECTED_VIEW_PDF_SORTING, DataStore.SELECTED_VIEW_PDF_SORTING_ORDER, DataStore.SELECTED_VIEW_PDF_SORTING_TITLE, DataStore.IS_CONTINOUS, DataStore.PAGE_BY_PAGE)
    annotation class DataStore {
        companion object {
            const val DATA_STORE_NAME = "com.datastore.name.deed"
            const val CURRENT_THEME = "com.theme.current.theme"
            const val SELECTED_APP_LANGUAGE = "com.language.current.language"
            const val SELECTED_VIEW_PDF_SORTING = "com.sorting.pdf.list"
            const val SELECTED_VIEW_PDF_SORTING_ORDER = "com.sorting.sorting.order"
            const val SELECTED_VIEW_PDF_SORTING_TITLE = "com.sorting.sorting.title"
            const val IS_CONTINOUS = "com.pdf.is.continous"
            const val PAGE_BY_PAGE = "com.is.page.by.page"
        }
    }

    @StringDef(DbConfiguration.DB_NAME)
    annotation class DbConfiguration {
        companion object {
            const val DB_NAME = "com.db.deed"
        }
    }

    @StringDef(SharedPrefrences.PREFRED_PDF_VIEW, SharedPrefrences.PREFRED_PDF_THEME, SharedPrefrences.PREFRENCE_NAME)
    annotation class SharedPrefrences {
        companion object {
            const val PREFRENCE_NAME = "com.prefrence.name.deed"
            const val PREFRED_PDF_VIEW = "com.pref.pdfView"
            const val PREFRED_PDF_THEME = "com.pref.theme"
        }
    }
}