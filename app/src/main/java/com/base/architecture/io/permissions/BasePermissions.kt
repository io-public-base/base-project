package com.base.architecture.io.permissions

import android.content.pm.PackageManager
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.base.architecture.io.eventbus.EventBus

object BasePermissions : LifecycleObserver {
    private lateinit var context: AppCompatActivity
    private lateinit var eventBus: EventBus


    private val writePermissions = android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    private lateinit var launcherPermissionUploadPhoto: ActivityResultLauncher<String>

    fun init(context: AppCompatActivity) {
        this.context = context
        this.eventBus = EventBus()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_CREATE)
    fun create() {
        registerCallbacks()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun destroy() {
    }

    private fun registerCallbacks() {
        launcherPermissionUploadPhoto =
            context.registerForActivityResult(ActivityResultContracts.RequestPermission()) {

            }
    }

    /**
     * upload photo
     */
    fun checkUploadPhotoPermission() = ContextCompat.checkSelfPermission(
        context,
        writePermissions
    ) == PackageManager.PERMISSION_GRANTED
}