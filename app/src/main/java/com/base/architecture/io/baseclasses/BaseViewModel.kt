package com.base.architecture.io.baseclasses

import androidx.lifecycle.ViewModel
import com.base.architecture.io.eventbus.EventBus
import java.text.SimpleDateFormat
import java.util.*

open class BaseViewModel : ViewModel() {

    var eventBus: EventBus = EventBus()

    fun getFormattedDate(dateVal: Long): String {
        try {
            var date = dateVal
            date *= 1000L
            return SimpleDateFormat("dd-MM-yyyy", Locale.US).format(Date(date))
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return ""
    }


}