package com.base.architecture.io.ui.fragments.demo


import android.os.Bundle
import android.view.*
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import com.base.architecture.io.BR
import com.base.architecture.io.R
import com.base.architecture.io.baseclasses.BaseFragment
import com.base.architecture.io.databinding.HomeFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.home_fragment.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


@AndroidEntryPoint
class DemoFragment : BaseFragment<HomeFragmentBinding, DemoViewModel>() {

    override val layoutId: Int
        get() = R.layout.home_fragment
    override val viewModel: Class<DemoViewModel>
        get() = DemoViewModel::class.java
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mViewDataBinding.mView.setOnClickListener{
            fetchDove()
        }
    }

    private fun fetchDove(){
        try {
            mViewModel.addDov("dov is created").observe(viewLifecycleOwner, Observer{
                showToast(it)
            })
        }catch (ex:Exception){
            throw ex
        }
    }
}