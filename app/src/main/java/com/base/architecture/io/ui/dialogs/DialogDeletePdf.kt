package com.base.architecture.io.ui.dialogs

import android.os.Bundle
import android.view.View
import com.base.architecture.io.R
import com.base.architecture.io.baseclasses.BaseDialog
import com.base.architecture.io.databinding.DialogDeletePdfBinding
import com.base.architecture.io.datalayer.repositories.ModuleOneRepo
import com.base.architecture.io.rawcollections.enumm.PdfActionsEnums
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class DialogDeletePdf(
    private val callback: (PdfActionsEnums) -> Unit
) : BaseDialog<DialogDeletePdfBinding>(){
    override val resLayout: Int
        get() = R.layout.dialog_delete_pdf

    @Inject
    lateinit var repositoryLocal: ModuleOneRepo

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("onViewCreated")

        binding.btnCancel.setOnClickListener {
            dismiss()
        }
        binding.btnDelPdf.setOnClickListener {

        }

    }

    override fun onStart() {
        super.onStart()
        setTopAnimation()
    }
}