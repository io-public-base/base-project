package com.base.architecture.io.rawcollections.enumm

enum class PdfActionsEnums {
    DELETE_PDF,
    PDF_EDITING_WARNING_EXIT,
    PDF_EDITING_WARNING_SAVE,
}