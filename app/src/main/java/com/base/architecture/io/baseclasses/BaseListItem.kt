package com.base.architecture.io.baseclasses

/**
 * extend this class for multi view recyclerView
 * this class represent data and layout of single item
 */
abstract class BaseListItem {
    abstract var layoutRes: Int
}