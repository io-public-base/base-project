package com.base.architecture.io.ui.fragments.demo.adapter

import android.view.View
import com.base.architecture.io.R
import com.base.architecture.io.baseclasses.BaseAdapter
import com.base.architecture.io.baseclasses.BaseListItem
import com.base.architecture.io.baseclasses.BaseViewHolder
import com.base.architecture.io.databinding.HomeFragmentBinding
import com.base.architecture.io.ui.fragments.demo.items.ListItemDemoMenu
import com.base.architecture.io.ui.fragments.demo.items.ListItemDemoTitle
import com.base.architecture.io.ui.fragments.demo.items.ListItemDemo
import timber.log.Timber

class DemoAdapter(private val list: MutableList<BaseListItem>) :
    BaseAdapter<HomeFragmentBinding, DemoAdapter.ViewHolder, BaseListItem>(list) {

    open class ViewHolder(view: View) : BaseViewHolder(view)


    class ViewItemDemoMenu(view: View) : ViewHolder(view) {
        fun bind(data: MutableList<String>) {

        }
    }

    class ViewHolderItemDemoTitle(view: View) : ViewHolder(view) {
        fun bind(data: String) {

        }
    }

    class ViewHolderItemDemo(view: View) : ViewHolder(view) {
        fun bind(data: String) {

        }
    }

    override val layoutId: Int
        get() = R.layout.home_fragment

    override fun bind(holder: ViewHolder, position: Int, data: BaseListItem) {
        Timber.d(holder.toString())
        when (holder) {
            is ViewHolderItemDemoTitle -> {
                holder.bind((data as ListItemDemoTitle).featured)
            }
            is ViewItemDemoMenu -> {
                holder.bind((data as ListItemDemoMenu).list)
            }
            is ViewHolderItemDemo -> {
                holder.bind((data as ListItemDemo).data)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return list[position].layoutRes
    }
}