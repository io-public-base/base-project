package com.base.architecture.io.ui.activity

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.navigation.NavigationView
import com.my_utils.utils.CACHE_SPLIT_DIR
import com.base.architecture.io.BR
import com.base.architecture.io.R
import com.base.architecture.io.baseclasses.BaseActivity
import com.base.architecture.io.databinding.ActivityMainBinding
import com.base.architecture.io.eventbus.messages.EventClickedToolbarBack
import com.base.architecture.io.eventbus.messages.EventOpenNavDrawer
import com.base.architecture.io.manager.LocalHelper
import dagger.hilt.android.AndroidEntryPoint
import org.greenrobot.eventbus.Subscribe
import java.io.File


@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var drawerLayout: DrawerLayout
    lateinit var navController: NavController
    private lateinit var navigationView: NavigationView
    private lateinit var headerLayout: View

//    @Inject
//    lateinit var dataStoreProvider: DataStoreProvider

    override val viewModel: Class<MainViewModel>
        get() = MainViewModel::class.java
    override val layoutId: Int
        get() = R.layout.activity_main
    override val bindingVariable: Int
        get() = BR.viewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        LocalHelper.onAttach(this)
        super.onCreate(savedInstanceState)
        setTheme(R.style.Theme_DeeD)
        initialising()
    }



    private fun initialising() {
        drawerLayout = findViewById(R.id.drawer_layout)
        navigationView = findViewById(R.id.nav_view)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_settings,
                R.id.nav_share,
                //R.id.nav_rate,
                R.id.nav_policy
            ), drawerLayout
        )
        navigationView.setupWithNavController(navController)



        //side menu
        val menuNav: Menu = navigationView.menu
        val navSettings = menuNav.findItem(R.id.nav_settings)
        val navShare = menuNav.findItem(R.id.nav_share)
       // val navRate = menuNav.findItem(R.id.nav_rate)
        val navPrivacyPolicy = menuNav.findItem(R.id.nav_policy)


        navSettings.setOnMenuItemClickListener {
            closeDrawerLayout()
            if (navController.currentDestination != null) {
                if (navController.currentDestination!!.label != "SettingsFragment") {
                   // navController.navigate(R.id.settingsFragment, null)
                }
            }
            return@setOnMenuItemClickListener true
        }

//        navRate.setOnMenuItemClickListener {
//            closeDrawerLayout()
//            if (navController.currentDestination != null) {
//                if (navController.currentDestination!!.label != "createPdf") {
//                    navController.navigate(R.id.createPdfFragment, null)
//                }
//            }
//            return@setOnMenuItemClickListener true
//        }

        navPrivacyPolicy.setOnMenuItemClickListener {
            closeDrawerLayout()
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("https://digizonetech.blogspot.com/2022/04/pdf-editor-privacy-policy.html"))
            startActivity(browserIntent)
            return@setOnMenuItemClickListener true
        }

        navShare.setOnMenuItemClickListener {
            closeDrawerLayout()
            try {

            } catch (e: java.lang.Exception) {
                //e.toString();
            }

            return@setOnMenuItemClickListener true
        }
    }
    private fun closeDrawerLayout() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    /**
     * subscribers
     */
    @Subscribe
    fun onEvent(event: EventClickedToolbarBack) {
        findNavController(R.id.nav_host_fragment).popBackStack()
    }

    @Subscribe
    fun onEvent(event: EventOpenNavDrawer) {
        drawerLayout.openDrawer(GravityCompat.START)
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            trimCache(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun trimCache(context: Context) {
        try {
            val dir: File? = File(context.cacheDir ,CACHE_SPLIT_DIR)
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir)
            }
        } catch (e: Exception) {
        }
    }

    fun deleteDir(dir: File?): Boolean {
        if (dir != null && dir.isDirectory()) {
            val children: Array<String> = dir.list()
            for (i in children.indices) {
                val success = deleteDir(File(dir, children[i]))
                if (!success) {
                    return false
                }
            }
        }

        // The directory is now empty so delete it
        return dir!!.delete()
    }


}