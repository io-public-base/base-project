package com.base.architecture.io.baseclasses

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.base.architecture.io.R
import com.base.architecture.io.ui.fragments.demo.adapter.DemoAdapter
import org.greenrobot.eventbus.EventBus
import java.lang.Exception

abstract class BaseAdapter<T : ViewDataBinding, VH : BaseViewHolder, D>(
    private val list: MutableList<D>
) : RecyclerView.Adapter<BaseViewHolder>() {

    lateinit var binding: T
    abstract val layoutId: Int
    abstract fun bind(holder: VH, position: Int, data: D)
    val eventBus = EventBus()

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view: View
        //check single view type recyclerview. if true then user layout id else use view type from sub adapter
        val type = if (viewType == VIEW_TYPE_SINGLE) {
            binding = DataBindingUtil.inflate(inflater, layoutId, parent, false)
            view = binding.root
            layoutId
        } else {
            view = inflater.inflate(viewType, parent, false)
            viewType
        }

        return when (type) {
            R.layout.demo_list -> DemoAdapter.ViewHolderItemDemo(view)
            R.layout.layout_demo_menu -> DemoAdapter.ViewItemDemoMenu(view)
            R.layout.list_item_demo_title -> DemoAdapter.ViewHolderItemDemoTitle(view)
            else -> throw Exception("No layout list item found in BaseAdapter")
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        bind(holder as VH, position, list[position])
    }

    /**
     * use this method for small list
     */
    fun setData(data: List<D>) {
        list.clear()
        list.addAll(data)
        notifyDataSetChanged()
    }

    companion object {
        const val VIEW_TYPE_SINGLE = 0
    }
}