package com.base.architecture.io.ui.fragments.demo.items

import com.base.architecture.io.R
import com.base.architecture.io.baseclasses.BaseListItem

class ListItemDemoMenu(val list: MutableList<String>) : BaseListItem() {
    override var layoutRes: Int = R.layout.layout_demo_menu
}