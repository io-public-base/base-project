package com.base.architecture.io.eventbus.messages

data class EventPdfPassword(
    val password : String
)