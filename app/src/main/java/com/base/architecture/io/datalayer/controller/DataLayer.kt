package com.base.architecture.io.datalayer.controller

import com.base.architecture.io.datalayer.repositories.ModuleOneRepo
import com.base.architecture.io.datalayer.repositories.ModuleSecondRepo
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class DataLayer @Inject constructor(
    private val transitionsRepository: ModuleOneRepo,
    private val reportsRepository: ModuleSecondRepo
){

    suspend fun makeTransitions(argument: String): String{
        return transitionsRepository.doTransition(argument)
    }

    fun makeDov(argument: String): String{
        return transitionsRepository.doDov(argument)
    }

    suspend fun fetchTransitions(argument: String){
        val response = reportsRepository.fetchReports(argument)
    }

    val updateReport: Flow<Any> = flow {
        emit(reportsRepository.makeReports("argument"))
    }
}