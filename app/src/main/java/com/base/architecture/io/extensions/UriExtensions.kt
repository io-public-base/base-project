package com.base.architecture.io.extensions

import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.util.concurrent.Executors
import kotlin.math.min

fun Activity.handlerViaRealPath(uri: String, callback: (String, String) -> Unit) {
    try {
        var fileUri = uri
        if (fileUri.isBlank()) {
            fileUri = getPathFromUri(intent.data!!).toString()
        }
        if (fileUri == "null") {
            Executors.newSingleThreadExecutor().execute(Runnable {
                try {
                    val result = getFilePathFromInputStream(intent.data!!)
                    handleViewInputStream(result){ filename, fileUri ->
                        callback(filename, fileUri)
                    }
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            })
        } else {
            val separator = "/"
            val arrValues: Array<String> = fileUri.split(separator).toTypedArray()
            val fileName = arrValues[arrValues.size - 1]

            callback(fileName, fileUri)

        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

fun Context.getPathFromUri(uri: Uri): String? {
    try {
        if (DocumentsContract.isDocumentUri(this, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId: String = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).toTypedArray()
                val type = split[0]
                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }


            }
            else if (isDownloadsDocument(uri)) {
                val id: String = DocumentsContract.getDocumentId(uri)
                val contentUri: Uri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"),
                    java.lang.Long.valueOf(id)
                )
                return getDataColumn(this, contentUri, null, null)
            }
            else if (isMediaDocument(uri)) {
                val docId: String = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).toTypedArray()
                val type = split[0]
                var contentUri: Uri? = null
                when (type) {
                    "image" -> {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    }
                    "video" -> {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    }
                    "audio" -> {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                }
                val selection = "_id=?"
                val selectionArgs = arrayOf(
                    split[1]
                )
                return getDataColumn(this, contentUri, selection, selectionArgs)
            }
        }
        else if ("content".equals(uri.scheme, ignoreCase = true)) {
            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
                this,
                uri,
                null,
                null
            )
        }
        else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    return null
}

fun Context.getFilePathFromInputStream(uri: Uri): JSONObject {
    val resultObj = JSONObject()
    try {
        val returnCursor = contentResolver.query(uri, null, null, null, null)
        val nameIndex = returnCursor!!.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        returnCursor.moveToFirst()
        val name = returnCursor.getString(nameIndex)
        val file = File(filesDir, name)
        try {
            val inputStream: InputStream? = contentResolver.openInputStream(uri)
            val outputStream = FileOutputStream(file)
            var read: Int
            val maxBufferSize = 1 * 1024 * 1024
            val bytesAvailable: Int = inputStream!!.available()
            val bufferSize = min(bytesAvailable, maxBufferSize)
            val buffers = ByteArray(bufferSize)
            while (inputStream.read(buffers).also { read = it } != -1) {
                outputStream.write(buffers, 0, read)
            }
            Log.e("File Size", "Size " + file.length())
            inputStream.close()
            outputStream.close()
            Log.e("File Path", "Path " + file.path)
            Log.e("File Size", "Size " + file.length())
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        resultObj.put("Name", file.name)
        resultObj.put("Path", file.path)
        returnCursor.close()
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    return resultObj
}

private fun handleViewInputStream(resultObj: JSONObject, callback: (String, String) -> Unit) {
    try {
        val fileUri: String

        if (resultObj.has("Name") && resultObj.has("Path")) {
            var fileName = resultObj.getString("Name")
            fileName = removeExtension(fileName)
            fileUri = resultObj.getString("Path")

            callback(fileName, fileUri)

        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

private fun getDataColumn(
    context: Context, uri: Uri?, selection: String?,
    selectionArgs: Array<String>?
): String? {
    try {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(
            column
        )
        try {
            cursor = context.contentResolver.query(
                uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index: Int = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    return null
}

fun removeExtension(fileName: String): String {
    return if(fileName.contains(".")) fileName.substring(0, fileName.lastIndexOf('.'))
    else fileName
}

fun Context.getFileParent(fileName: String, splitter: String): String {
    return if(fileName.contains(splitter)) fileName.substring(0, fileName.lastIndexOf(splitter))
    else fileName
}



private fun isExternalStorageDocument(uri: Uri): Boolean {
    return "com.android.externalstorage.documents" == uri.authority
}

private fun isDownloadsDocument(uri: Uri): Boolean {
    return "com.android.providers.downloads.documents" == uri.authority
}

private fun isMediaDocument(uri: Uri): Boolean {
    return "com.android.providers.media.documents" == uri.authority
}

private fun isGooglePhotosUri(uri: Uri): Boolean {
    return "com.google.android.apps.photos.content" == uri.authority
}