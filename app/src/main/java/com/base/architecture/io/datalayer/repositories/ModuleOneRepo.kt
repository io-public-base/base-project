package com.base.architecture.io.datalayer.repositories

import com.base.architecture.io.datalayer.local.database.AppDatabase
import javax.inject.Inject

class ModuleOneRepo @Inject constructor(appDatabase: AppDatabase) {
    private val dataSource = appDatabase.datasource()


    suspend fun doTransition(string: String) = dataSource.transit(string)

    fun doDov(string: String): String {
        dataSource.dov(string)
        return string
    }
}