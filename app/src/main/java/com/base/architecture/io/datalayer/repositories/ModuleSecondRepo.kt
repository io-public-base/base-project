package com.base.architecture.io.datalayer.repositories

import com.base.architecture.io.datalayer.local.database.AppDatabase
import javax.inject.Inject

class ModuleSecondRepo @Inject constructor(appDatabase: AppDatabase) {

    val dataSource = appDatabase.datasource()

    suspend fun fetchReports(string: String) : String{return string}
    suspend fun makeReports(string: String) : String{return string}


}