package com.base.architecture.io.shared

class PublicValue {

    companion object{
        const val KEY_REQUEST_PERMISSIONS = 0x0001
        const val KEY_SAVING_REQUEST_PERMISSIONS = 0x009
        const val KEY_REQUEST_PERMISSIONS_11 = 0x100011
        const val KEY_REQUEST_FILE_PICKER = 0x10002

        const val DEFAULT_PAGE_NUMBER = 0
        const val KEY_REFERENCE_HASH = "ReferenceHash:"

        const val DARK_THEME = "theme.dark"
        const val DEFAULT_THEME = "theme.default"


        const val APP_LANGUAGE_TURKCE = "tr"
        const val APP_DEFAULT_LANGUAGE = "en"
    }
}