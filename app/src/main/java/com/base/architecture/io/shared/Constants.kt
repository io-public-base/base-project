package com.my_utils.utils

import android.Manifest
import android.provider.MediaStore
import android.provider.MediaStore.Files.FileColumns.DATA
import android.provider.MediaStore.Files.FileColumns.MIME_TYPE
import android.webkit.MimeTypeMap


const val CACHE_SPLIT_DIR = "/" + "com.cache.split"