package com.my_utils.utils

import android.app.Activity
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import org.json.JSONObject
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import kotlin.math.min


fun removeExtension(fileName: String): String {
    return if (fileName.contains(".")) fileName.substring(0, fileName.lastIndexOf('.'))
    else fileName
}

fun Context.getName(uri: Uri): JSONObject {
    val result = JSONObject()
    var name = ""
    var path = ""

    runCatching {
        uri.scheme?.let { scheme ->

            if (scheme == "file") {
                name = uri.lastPathSegment.toString()
                path = uri.path ?: ""
            } else if (scheme == "content") {
                val projection = arrayOf(
                    MediaStore.Files.FileColumns.TITLE,
                    MediaStore.Files.FileColumns.DISPLAY_NAME,
                    MediaStore.Files.FileColumns.RELATIVE_PATH
                )
                val cursor: Cursor? = contentResolver.query(uri, projection, null, null, null)
                if (cursor != null && cursor.count > 0) {
                    val columnIndex1: Int =
                        cursor.getColumnIndex(MediaStore.Files.FileColumns.TITLE)
                    val columnIndex: Int =
                        cursor.getColumnIndex(MediaStore.Files.FileColumns.DISPLAY_NAME)
                    val columnIndexPath: Int =
                        cursor.getColumnIndex(MediaStore.Files.FileColumns.RELATIVE_PATH)
                    cursor.moveToFirst()

                    if (columnIndex >= 0) name = cursor.getString(columnIndex)
                    if (name.isEmpty()) if (columnIndex1 >= 0) name = cursor.getString(columnIndex1)

                    if (columnIndexPath >= 0) path = cursor.getString(columnIndexPath)

                }
                cursor?.close()
            }
        }

        result.put("Name", name)
        result.put("Path", path)

    }.onFailure { e -> e.printStackTrace() }

    return result

}

fun Context.getRealPathFromURI(contentUri: Uri?): String {
    try {
        val projection = arrayOf(MediaStore.Files.FileColumns.DATA)

        if (contentUri != null) {
            contentResolver.query(contentUri, projection, null, null, null).use {
                if (it != null) {
                    val columnIndex = it.getColumnIndex(MediaStore.Files.FileColumns.DATA)
                    it.moveToFirst()
                    if (columnIndex >= 0) return it.getString(columnIndex)
                }
            }
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    return ""
}

fun removePdfExtension(fileName: String, seperator: String): String {
    return if(fileName.contains(seperator)) fileName.substring(0, fileName.lastIndexOf(seperator))
    else fileName
}


fun Activity.handlerViaRealPath(uri: String, callback: (String, String, String) -> Unit) {
    try {
        var fileUri = uri
        if (fileUri.isBlank()) {
            fileUri = intent.data?.let { getPathFromUri(it).toString() } ?: "null"
        }
        if (fileUri == "null") {
            CoroutineScope(Dispatchers.Main).launch {
                try {
                    val result = async(Dispatchers.IO) {
                        intent.data?.let { getFilePathFromInputStream(it) } ?: JSONObject()
                    }
                    handleViewInputStream(result.await()) { filename, fileUri, fileSize ->
                        callback(filename, fileUri, fileSize)
                    }
                } catch (ex: java.lang.Exception) {
                    ex.printStackTrace()
                }
            }
        } else {
            CoroutineScope(Dispatchers.Main).launch {
                val file = File(fileUri)
                val fileName = file.name
                val fileSize = file.length()

                callback(fileName, fileUri, fileSize.toString())
            }
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

fun Context.getPathFromUri(uri: Uri): String? {
    try {


        if (DocumentsContract.isDocumentUri(this, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId: String = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).toTypedArray()
                val type = split[0]
                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }

                //  handle non-primary volumes
            } else if (isDownloadsDocument(uri)) {
                val id: String = DocumentsContract.getDocumentId(uri)
                val contentUri: Uri = ContentUris.withAppendedId(
                    Uri.parse("content://downloads/public_downloads"),
                    java.lang.Long.valueOf(id)
                )
                return getDataColumn(this, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId: String = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).toTypedArray()
                val type = split[0]
                var contentUri: Uri? = null
                when (type) {
                    "image" -> {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    }
                    "video" -> {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    }
                    "audio" -> {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                }
                val selection = "_id=?"
                val selectionArgs = arrayOf(
                    split[1]
                )
                return getDataColumn(this, contentUri, selection, selectionArgs)
            }
        } else if ("content".equals(uri.scheme, ignoreCase = true)) {
            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
                this,
                uri,
                null,
                null
            )
        } else if ("file".equals(uri.scheme, ignoreCase = true)) {
            return uri.path
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    return null
}

fun Context.getFilePathFromInputStream(uri: Uri): JSONObject {
    val resultObj = JSONObject()
    try {


        contentResolver.query(uri, null, null, null, null)?.use {

            val nameIndex = it.getColumnIndex(OpenableColumns.DISPLAY_NAME)
            it.moveToFirst()

            val name = it.getString(nameIndex)
            val file = File(filesDir, name)

            try {
                val inputStream: InputStream? = contentResolver.openInputStream(uri)
                val outputStream = FileOutputStream(file)
                var read: Int
                val maxBufferSize = 1 * 1024 * 1024
                val bytesAvailable: Int = inputStream?.available() ?: 0
                val bufferSize = min(bytesAvailable, maxBufferSize)
                val buffers = ByteArray(bufferSize)
                inputStream?.let {
                    while (inputStream.read(buffers).also { read = it } != -1) {
                        outputStream.write(buffers, 0, read)
                    }
                    inputStream.close()
                }
                outputStream.close()
                Log.e("File Path", "Path " + file.path)
                Log.e("File Size", "Size " + file.length())
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }

            resultObj.put("Name", file.name)
            resultObj.put("Path", file.path)
            resultObj.put("Size", file.length())

        }

    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    return resultObj
}

private fun handleViewInputStream(
    resultObj: JSONObject,
    callback: (String, String, String) -> Unit
) {
    try {
        val fileUri: String


        if (resultObj.has("Name") && resultObj.has("Path") && resultObj.has("Size")) {
            var fileName = resultObj.getString("Name")
            fileName = removeExtension(fileName)
            fileUri = resultObj.getString("Path")
            val fileSize = resultObj.getLong("Size").toString()

            callback(fileName, fileUri, fileSize)

        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
}

private fun removeLastChars(str: String, chars: Int = 4): String {
    if (str.isEmpty()) return ""
    return str.substring(0, str.length - chars)
}

private fun getDataColumn(
    context: Context, uri: Uri?, selection: String?,
    selectionArgs: Array<String>?
): String? {
    try {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)
        try {
            cursor = uri?.let {
                context.contentResolver.query(it, projection, selection, selectionArgs, null)
            }

            if (cursor != null && cursor.moveToFirst()) {
                val index: Int = cursor.getColumnIndex(column)
                if (index >= 0) return cursor.getString(index)
            }

        } finally {
            cursor?.close()
        }
    } catch (ex: Exception) {
        ex.printStackTrace()
    }
    return null
}

private fun isExternalStorageDocument(uri: Uri): Boolean {
    return "com.android.externalstorage.documents" == uri.authority
}

private fun isDownloadsDocument(uri: Uri): Boolean {
    return "com.android.providers.downloads.documents" == uri.authority
}

private fun isMediaDocument(uri: Uri): Boolean {
    return "com.android.providers.media.documents" == uri.authority
}

private fun isGooglePhotosUri(uri: Uri): Boolean {
    return "com.google.android.apps.photos.content" == uri.authority
}


//region Used for Android 10,11 Print issue
fun Context.getFile(uri: Uri): File {
    val destinationFilename = File(filesDir.path + File.separatorChar + queryName(uri))
    try {
        contentResolver.openInputStream(uri).use { ins ->
            createFileFromStream(
                ins!!,
                destinationFilename
            )
        }
    } catch (ex: java.lang.Exception) {
        Log.e("Save File", ex.message.toString())
        ex.printStackTrace()
    }
    return destinationFilename
}

private fun createFileFromStream(ins: InputStream, destination: File?) {
    try {
        FileOutputStream(destination).use { os ->
            val buffer = ByteArray(4096)
            var length: Int
            while (ins.read(buffer).also { length = it } > 0) {
                os.write(buffer, 0, length)
            }
            os.flush()
        }
    } catch (ex: Exception) {
        Log.e("Save File", ex.message.toString())
        ex.printStackTrace()
    }
}

private fun Context.queryName(uri: Uri): String {
    contentResolver.query(uri, null, null, null, null)?.use {
        val nameIndex = it.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        it.moveToFirst()
        val name = it.getString(nameIndex)
        it.close()
        return name
    }
    return "";
}
//endregion
