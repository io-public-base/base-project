package com.base.architecture.io.rawcollections.enumm;

public enum AnnotColors {
    colorBlack,
    colorRed,
    colorYellow,
    colorGreen,
    colorBlue,

    colorBlackH,
    colorRedH,
    colorYellowH,
    colorGreenH,
    colorBlueH,
}
