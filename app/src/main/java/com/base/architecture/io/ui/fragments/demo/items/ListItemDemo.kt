package com.base.architecture.io.ui.fragments.demo.items

import com.base.architecture.io.R
import com.base.architecture.io.baseclasses.BaseListItem


class ListItemDemo(val data: String) : BaseListItem() {
    override var layoutRes: Int = R.layout.demo_list
}