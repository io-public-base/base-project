package com.base.architecture.io.ui.fragments.demo.menuadapter

import android.view.View
import com.base.architecture.io.R
import com.base.architecture.io.baseclasses.BaseAdapter
import com.base.architecture.io.baseclasses.BaseViewHolder
import com.base.architecture.io.databinding.ListItemMenuItemsBinding

class MenuAdapter(private val list: MutableList<String>) :
    BaseAdapter<ListItemMenuItemsBinding, MenuAdapter.ViewHolder, String>(list) {
    class ViewHolder(view: View) : BaseViewHolder(view)

    override val layoutId: Int
        get() = R.layout.list_item_menu_items

    override fun bind(holder: ViewHolder, position: Int, data: String) {

    }
}