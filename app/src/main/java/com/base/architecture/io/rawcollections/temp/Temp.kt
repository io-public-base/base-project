package com.base.architecture.io.rawcollections.temp

import android.widget.TextView

class Temp {
    private  var sortingTemp: TextView? = null
    fun setsortingTemp(temp: TextView){
        sortingTemp = temp
    }

    fun getSortingTemp () = sortingTemp
}