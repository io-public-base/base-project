package com.base.architecture.io.datalayer.prefrencedatasource.sharedprefrence

import android.content.Context
import android.content.SharedPreferences
import com.base.architecture.io.shared.AppConstants
import com.base.architecture.io.shared.PublicValue

class Storage(context: Context) {
    private var preferences: SharedPreferences = context.getSharedPreferences(AppConstants.SharedPrefrences.PREFRENCE_NAME, Context.MODE_PRIVATE)

    fun setPreferredPdfView(boo: Boolean){
        preferences.edit().putBoolean(AppConstants.SharedPrefrences.PREFRED_PDF_VIEW, boo).apply()
    }

    fun getPreferredPdfView(): Boolean {
        return preferences.getBoolean(AppConstants.SharedPrefrences.PREFRED_PDF_VIEW, false)!!
    }

    fun getPreferredTheme(): String {
        return preferences.getString(AppConstants.SharedPrefrences.PREFRED_PDF_THEME, PublicValue.DEFAULT_THEME)!!
    }

    fun setPreferredTheme(string: String){
        preferences.edit().putString(AppConstants.SharedPrefrences.PREFRED_PDF_THEME, string).apply()
    }


}