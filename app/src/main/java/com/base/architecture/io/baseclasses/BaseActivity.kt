package com.base.architecture.io.baseclasses

import android.app.Dialog
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProviders
import com.base.architecture.io.eventbus.EventBus
import com.base.architecture.io.permissions.BasePermissions
import java.lang.Exception


abstract class BaseActivity<T : ViewDataBinding, V : BaseViewModel>  : AppCompatActivity() {

    private lateinit var mViewDataBinding: T
    protected lateinit var mViewModel: V
    protected lateinit var loadingDialog: Dialog



    /**
     * viewModel variable that will get value from activity which it will implement this
     * we will use this variable viewModel to bind with view through databinding
     */
    abstract val viewModel: Class<V>

    /**
     * layoutId variable to get layout value from activity which will implement this layoutId
     * we will use this layoutId for databinding
     */
    @get:LayoutRes
    abstract val layoutId: Int

    /**
     * bindingVariable which will bind with view
     */

    abstract val bindingVariable: Int

    /**
     * eventbus
     */
    lateinit var pdfEventBus: EventBus

    //@Inject lateinit var dataStoreProvider: DataStoreProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initLifeCycleObservers()
        databindingWithViewModel()
        subscribeToNetworkLiveData()
        subscribeToViewLiveData()
        initEventBus()
    }

    /**
     * Function to perform databinding and attaching viewmodel with view
     */

    private fun databindingWithViewModel() {
        try {
            mViewDataBinding = DataBindingUtil.setContentView(this, layoutId)
            mViewModel = ViewModelProviders.of(this).get(viewModel)
            mViewDataBinding.setVariable(bindingVariable, mViewModel)
            mViewDataBinding.executePendingBindings()
        }catch (ex: Exception){

        }
    }

    override fun onStart() {
        super.onStart()
        initEventBus()
    }

    override fun onStop() {
        super.onStop()
        pdfEventBus.unsubscribe(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        pdfEventBus.unsubscribe(this)
    }

    open fun subscribeToNetworkLiveData() {}

    open fun subscribeToViewLiveData() {}

    open fun initLifeCycleObservers() {
        BasePermissions.init(this)
        lifecycle.addObserver(BasePermissions)
    }

    /**
     * event bus subscribers
     */
    private fun initEventBus() {
        pdfEventBus = EventBus()
        pdfEventBus.subscribe(this)
    }

}

