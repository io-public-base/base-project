package com.base.architecture.io.manager

import android.content.Context
import android.content.ContextWrapper
import android.os.Build
import java.util.*

class LocalHelper(base: Context) : ContextWrapper(base) {

    companion object {


        private  val SELECTED_LANGUAGE = "Locale.Helper.Selected.Language"
        private  val LANGUAGE_PREF = "language_pref"

//        fun updateLocale(c: Context, localeToSwitchTo: Locale): ContextWrapper {
//            var context = c
//            val resources: Resources = context.resources
//            val configuration: Configuration = resources.configuration
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                val localeList = LocaleList(localeToSwitchTo)
//                LocaleList.setDefault(localeList)
//                configuration.setLocales(localeList)
//            } else {
//                configuration.locale = localeToSwitchTo
//            }
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
//                context = context.createConfigurationContext(configuration)
//            } else {
//                resources.updateConfiguration(configuration, resources.displayMetrics)
//            }
//            return LocalHelper(context)
//        }

        fun onAttach(context: Context): Context {
            return onAttach(context, Locale.getDefault().language)
        }

        private fun onAttach(context: Context, defaultLanguage: String): Context {
            val lang = getPersistedData(context, defaultLanguage)
            return context.setLocale(lang)
        }

        fun getLanguage(context: Context): String {
            return getPersistedData(context, Locale.getDefault().language)
        }

        fun Context.setLocale(language: String): Context {
            persist(this, language)
            return updateResourcesLegacy(language)
        }

        private fun getPersistedData(context: Context, defaultLanguage: String): String {
            val preferences = context.getSharedPreferences(LANGUAGE_PREF, Context.MODE_PRIVATE)
            return preferences.getString(SELECTED_LANGUAGE, defaultLanguage) ?: "en"
        }

        private fun persist(context: Context, language: String?) {
            val preferences = context.getSharedPreferences(LANGUAGE_PREF, Context.MODE_PRIVATE)
            val editor = preferences.edit()
            editor.putString(SELECTED_LANGUAGE, language)
            editor.apply()
        }

        private fun Context.updateResourcesLegacy(language: String): Context {
            val locale = Locale(language)
            Locale.setDefault(locale)
            val resources = resources
            val configuration = resources.configuration
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) configuration.setLocale(locale)
            else configuration.locale = locale
            configuration.setLayoutDirection(locale)
            resources.updateConfiguration(configuration, resources.displayMetrics)
            return this
        }
    }






/*
    @TargetApi(Build.VERSION_CODES.N)
    private fun Context.updateResources(language: String): Context {
        val locale = Locale(language)
        Locale.setDefault(locale)
        val configuration = resources.configuration
        configuration.setLocale(locale)
        configuration.setLayoutDirection(locale)
        return createConfigurationContext(configuration)
    }
*/


}