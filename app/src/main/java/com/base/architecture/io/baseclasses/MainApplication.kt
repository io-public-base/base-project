package com.base.architecture.io.baseclasses

import android.app.Application
import android.content.Context
import android.util.Log
import com.base.architecture.io.datalayer.prefrencedatasource.sharedprefrence.Storage
import com.base.architecture.io.manager.LocalHelper
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApplication : Application(){


    val storage : Storage by lazy {
        Storage(this)
    }




    override fun onCreate() {
        super.onCreate()
        Log.i("initiate", "App is launched!")
    }

    override fun attachBaseContext(base: Context) {
        val preferences = base.getSharedPreferences("language_pref", Context.MODE_PRIVATE)
        val l = preferences?.getString("Locale.Helper.Selected.Language", "en")
        Log.i("initiate", " language------------------------------"+l.toString())
        super.attachBaseContext(LocalHelper.onAttach(base))
    }

//    override fun attachBaseContext(base: Context) {
//        val preferences = base.getSharedPreferences("language_pref", Context.MODE_PRIVATE)
//        val l = preferences?.getString("Locale.Helper.Selected.Language", "en")
//        Log.i("initiate", " language------------------------------"+l.toString())
//        super.attachBaseContext(LocaleUtils.getLocalizedContext(base, Storage(base).getPreferredLocale()))
//    }


}