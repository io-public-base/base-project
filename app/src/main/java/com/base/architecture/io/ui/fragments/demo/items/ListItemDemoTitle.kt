package com.base.architecture.io.ui.fragments.demo.items

import com.base.architecture.io.R
import com.base.architecture.io.baseclasses.BaseListItem


class ListItemDemoTitle(val featured: String) : BaseListItem() {
    override var layoutRes: Int = R.layout.list_item_demo_title
}