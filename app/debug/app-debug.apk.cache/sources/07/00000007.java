package com.base.architecture.io.managepdf;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.widget.Toast;
import java.util.List;
import kotlin.Metadata;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.BuildersKt__Builders_commonKt;
import kotlinx.coroutines.CoroutineScopeKt;
import kotlinx.coroutines.Dispatchers;
import org.apache.fontbox.ttf.NamingTable;

/* compiled from: MergePdf.kt */
@Metadata(d1 = {"\u0000B\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010!\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0010\u000b\n\u0002\u0018\u0002\n\u0002\b\u0005\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J_\u0010\u0003\u001a\u00020\u00042\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\u0006\u0010\b\u001a\u00020\u00072\u0006\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00072!\u0010\u0010\u001a\u001d\u0012\u0013\u0012\u00110\u0012¢\u0006\f\b\u0013\u0012\b\b\u0014\u0012\u0004\b\b(\u0015\u0012\u0004\u0012\u00020\u00040\u0011J\u0018\u0010\u0016\u001a\u00020\u00042\u0006\u0010\u0017\u001a\u00020\u00072\u0006\u0010\r\u001a\u00020\u000eH\u0016¨\u0006\u0018"}, d2 = {"Lcom/pdf/reader/deed/managepdf/MergePdf;", "", "()V", "execute", "", "pList", "", "", "fileName", "activity", "Landroid/app/Activity;", "loadingDialog", "Landroid/app/Dialog;", "ctx", "Landroid/content/Context;", "parentFile", "callBacK", "Lkotlin/Function1;", "", "Lkotlin/ParameterName;", NamingTable.TAG, "result", "showToast", "message", "app_debug"}, k = 1, mv = {1, 5, 1}, xi = 48)
/* loaded from: classes9.dex */
public final class MergePdf {
    public static final MergePdf INSTANCE = new MergePdf();

    private MergePdf() {
    }

    public final void execute(List<String> pList, String fileName, Activity activity, Dialog loadingDialog, Context ctx, String parentFile, Function1<? super Boolean, Unit> callBacK) {
        Intrinsics.checkNotNullParameter(pList, "pList");
        Intrinsics.checkNotNullParameter(fileName, "fileName");
        Intrinsics.checkNotNullParameter(activity, "activity");
        Intrinsics.checkNotNullParameter(loadingDialog, "loadingDialog");
        Intrinsics.checkNotNullParameter(ctx, "ctx");
        Intrinsics.checkNotNullParameter(parentFile, "parentFile");
        Intrinsics.checkNotNullParameter(callBacK, "callBacK");
        try {
            BuildersKt__Builders_commonKt.launch$default(CoroutineScopeKt.CoroutineScope(Dispatchers.getIO()), null, null, new MergePdf$execute$1(activity, ctx, fileName, pList, parentFile, loadingDialog, callBacK, null), 3, null);
        } catch (Exception e) {
            loadingDialog.dismiss();
            callBacK.mo2330invoke(false);
        }
    }

    public void showToast(String message, Context ctx) {
        Intrinsics.checkNotNullParameter(message, "message");
        Intrinsics.checkNotNullParameter(ctx, "ctx");
        Toast.makeText(ctx, message, 0).show();
    }
}