package com.base.architecture.io.anchors;

import java.util.ArrayList;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: PdfPaths.kt */
@Metadata(d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B5\u0012\u0016\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0004\u0012\u0006\u0010\u0007\u001a\u00020\u0004\u0012\u0006\u0010\b\u001a\u00020\u0004¢\u0006\u0002\u0010\tJ\u0019\u0010\u0010\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005HÆ\u0003J\t\u0010\u0011\u001a\u00020\u0004HÆ\u0003J\t\u0010\u0012\u001a\u00020\u0004HÆ\u0003J\t\u0010\u0013\u001a\u00020\u0004HÆ\u0003JA\u0010\u0014\u001a\u00020\u00002\u0018\b\u0002\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u00042\b\b\u0002\u0010\u0007\u001a\u00020\u00042\b\b\u0002\u0010\b\u001a\u00020\u0004HÆ\u0001J\u0013\u0010\u0015\u001a\u00020\u00162\b\u0010\u0017\u001a\u0004\u0018\u00010\u0001HÖ\u0003J\t\u0010\u0018\u001a\u00020\u0019HÖ\u0001J\t\u0010\u001a\u001a\u00020\u0004HÖ\u0001R!\u0010\u0002\u001a\u0012\u0012\u0004\u0012\u00020\u00040\u0003j\b\u0012\u0004\u0012\u00020\u0004`\u0005¢\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0007\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\f\u0010\rR\u0011\u0010\b\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000e\u0010\rR\u0011\u0010\u0006\u001a\u00020\u0004¢\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\r¨\u0006\u001b"}, d2 = {"Lcom/pdf/reader/deed/anchors/PdfPaths;", "", "absolutePathList", "Ljava/util/ArrayList;", "", "Lkotlin/collections/ArrayList;", "password", "fileName", "parentFile", "(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V", "getAbsolutePathList", "()Ljava/util/ArrayList;", "getFileName", "()Ljava/lang/String;", "getParentFile", "getPassword", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "app_debug"}, k = 1, mv = {1, 5, 1}, xi = 48)
/* loaded from: classes8.dex */
public final class PdfPaths {
    private final ArrayList<String> absolutePathList;
    private final String fileName;
    private final String parentFile;
    private final String password;

    /* JADX WARN: Multi-variable type inference failed */
    public static /* synthetic */ PdfPaths copy$default(PdfPaths pdfPaths, ArrayList arrayList, String str, String str2, String str3, int i, Object obj) {
        if ((i & 1) != 0) {
            arrayList = pdfPaths.absolutePathList;
        }
        if ((i & 2) != 0) {
            str = pdfPaths.password;
        }
        if ((i & 4) != 0) {
            str2 = pdfPaths.fileName;
        }
        if ((i & 8) != 0) {
            str3 = pdfPaths.parentFile;
        }
        return pdfPaths.copy(arrayList, str, str2, str3);
    }

    public final ArrayList<String> component1() {
        return this.absolutePathList;
    }

    public final String component2() {
        return this.password;
    }

    public final String component3() {
        return this.fileName;
    }

    public final String component4() {
        return this.parentFile;
    }

    public final PdfPaths copy(ArrayList<String> absolutePathList, String password, String fileName, String parentFile) {
        Intrinsics.checkNotNullParameter(absolutePathList, "absolutePathList");
        Intrinsics.checkNotNullParameter(password, "password");
        Intrinsics.checkNotNullParameter(fileName, "fileName");
        Intrinsics.checkNotNullParameter(parentFile, "parentFile");
        return new PdfPaths(absolutePathList, password, fileName, parentFile);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof PdfPaths)) {
            return false;
        }
        PdfPaths pdfPaths = (PdfPaths) obj;
        return Intrinsics.areEqual(this.absolutePathList, pdfPaths.absolutePathList) && Intrinsics.areEqual(this.password, pdfPaths.password) && Intrinsics.areEqual(this.fileName, pdfPaths.fileName) && Intrinsics.areEqual(this.parentFile, pdfPaths.parentFile);
    }

    public int hashCode() {
        return (((((this.absolutePathList.hashCode() * 31) + this.password.hashCode()) * 31) + this.fileName.hashCode()) * 31) + this.parentFile.hashCode();
    }

    public String toString() {
        return "PdfPaths(absolutePathList=" + this.absolutePathList + ", password=" + this.password + ", fileName=" + this.fileName + ", parentFile=" + this.parentFile + ')';
    }

    public PdfPaths(ArrayList<String> absolutePathList, String password, String fileName, String parentFile) {
        Intrinsics.checkNotNullParameter(absolutePathList, "absolutePathList");
        Intrinsics.checkNotNullParameter(password, "password");
        Intrinsics.checkNotNullParameter(fileName, "fileName");
        Intrinsics.checkNotNullParameter(parentFile, "parentFile");
        this.absolutePathList = absolutePathList;
        this.password = password;
        this.fileName = fileName;
        this.parentFile = parentFile;
    }

    public final ArrayList<String> getAbsolutePathList() {
        return this.absolutePathList;
    }

    public final String getPassword() {
        return this.password;
    }

    public final String getFileName() {
        return this.fileName;
    }

    public final String getParentFile() {
        return this.parentFile;
    }
}