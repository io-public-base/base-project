package com.base.architecture.io.managepdf;

import kotlin.Metadata;
import kotlin.jvm.internal.DefaultConstructorMarker;

/* compiled from: AnnotationConstants.kt */
@Metadata(d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005¢\u0006\u0002\u0010\u0002¨\u0006\u0004"}, d2 = {"Lcom/pdf/reader/deed/managepdf/AnnotationConstants;", "", "()V", "Companion", "app_debug"}, k = 1, mv = {1, 5, 1}, xi = 48)
/* loaded from: classes9.dex */
public final class AnnotationConstants {
    public static final String CACHE_MERGED_DIR = "/com.cache.merged";
    public static final String CACHE_SPLIT_DIR = "/com.cache.split";
    public static final String CMD_CHANGE_COL = "(function(){sel = window.getSelection();  if (sel.rangeCount && sel.getRangeAt) { range = sel.getRangeAt(0);  }  document.designMode = 'on';  if (range) { sel.removeAllRanges();  sel.addRange(range);  }  document.execCommand('foreColor', false, 'red');  document.designMode = 'off';})()";
    public static final String CMD_ENABLE_EDITING = "(function(){document.getElementsByTagName('body')[0].contentEditable = 'true';  setTimeout(function(){ document.querySelectorAll('.t').forEach(function(sp){sp.style.border = '4px dashed rgb(60 179 113)'; }, 100); document.querySelector('#sidebar')?.remove();})})()";
    public static final String CMD_HIGHLIGHTER = "(function(){sel = window.getSelection();  if (sel.rangeCount && sel.getRangeAt) { range = sel.getRangeAt(0);  }  document.designMode = 'on';  if (range) { sel.removeAllRanges();  sel.addRange(range);  }  document.execCommand('backColor', false, 'yellow');  document.designMode = 'off';})()";
    public static final String CMD_ST_BOLD = "(function(){sel = window.getSelection();  if (sel.rangeCount && sel.getRangeAt) { range = sel.getRangeAt(0);  }  document.designMode = 'on';  if (range) { sel.removeAllRanges();  sel.addRange(range);  }  document.execCommand('bold', false, 'red');  document.designMode = 'off';})()";
    public static final String CMD_TX_ITALIC = "(function(){sel = window.getSelection();  if (sel.rangeCount && sel.getRangeAt) { range = sel.getRangeAt(0);  }  document.designMode = 'on';  if (range) { sel.removeAllRanges();  sel.addRange(range);  }  document.execCommand('italic', false, 'red');  document.designMode = 'off';})()";
    public static final String CMD_UNDERLINE_TX = "(function(){sel = window.getSelection();  if (sel.rangeCount && sel.getRangeAt) { range = sel.getRangeAt(0);  }  document.designMode = 'on';  if (range) { sel.removeAllRanges();  sel.addRange(range);  }  document.execCommand('underline', false, 'red');  document.designMode = 'off';})()";
    public static final String COMPRESS_PDF_PATH = "/DeeD Compressed";
    public static final Companion Companion = new Companion(null);
    public static final String DISABLE_EDITING = "(function(){document.getElementsByTagName('body')[0].contentEditable = 'false'})()";
    public static final String DISABLE_EDITING_BORDERS = "(function(){setTimeout(function(){ document.querySelectorAll('.t').forEach(function(sp){sp.style.border = '0px dashed rgb(60 179 113)'; }, 10); })})()";
    public static final String FILE_SAVE_PATH = "/Stock Transfer";
    public static final String GET_SELECTION = "(function(){return document.getSelection();})()";
    public static final String MERGED_PDF_PATH = "/DeeD MERGED";
    public static final String SCALE_PDF_VIEW = "function myFunction(x){if (x.matches) {document.getElementsByTagName('body')[0].style.backgroundColor = 'yellow';} else {document.getElementsByTagName('body')[0].style.backgroundColor = 'pink';} } var x = window.matchMedia('(min-width: 100px)'); myFunction(x);  x.addListener(myFunction)";
    public static final String SELF_INVOKING_FUN = "(function(){document.querySelector('#sidebar').remove();})()";
    public static final String SPLIT_PDF_PATH = "/DeeD Splitter";

    /* compiled from: AnnotationConstants.kt */
    @Metadata(d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0011\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0011\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0012\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0013\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000R\u000e\u0010\u0014\u001a\u00020\u0004X\u0086T¢\u0006\u0002\n\u0000¨\u0006\u0015"}, d2 = {"Lcom/pdf/reader/deed/managepdf/AnnotationConstants$Companion;", "", "()V", "CACHE_MERGED_DIR", "", "CACHE_SPLIT_DIR", "CMD_CHANGE_COL", "CMD_ENABLE_EDITING", "CMD_HIGHLIGHTER", "CMD_ST_BOLD", "CMD_TX_ITALIC", "CMD_UNDERLINE_TX", "COMPRESS_PDF_PATH", "DISABLE_EDITING", "DISABLE_EDITING_BORDERS", "FILE_SAVE_PATH", "GET_SELECTION", "MERGED_PDF_PATH", "SCALE_PDF_VIEW", "SELF_INVOKING_FUN", "SPLIT_PDF_PATH", "app_debug"}, k = 1, mv = {1, 5, 1}, xi = 48)
    /* loaded from: classes9.dex */
    public static final class Companion {
        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        private Companion() {
        }
    }
}