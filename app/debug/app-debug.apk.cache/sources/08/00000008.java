package com.base.architecture.io.managepdf;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.pdfbox.cos.COSArray;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.cos.COSObject;
import org.apache.pdfbox.cos.COSStream;
import org.apache.pdfbox.pdmodel.PDDocument;

/* loaded from: classes9.dex */
public class OptimisePdf {

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes9.dex */
    public interface Reference {
        COSBase getFrom();

        COSBase getTo();

        void setTo(COSBase to);
    }

    public OptimisePdf(String absolutePath, String p) throws IOException {
        PDDocument pdDocument1 = PDDocument.load(new File(absolutePath));
        try {
            try {
                Map<COSBase, Collection<Reference>> complexObjects = findComplexObjects(pdDocument1);
                int pass = 0;
                while (true) {
                    int merges = 0;
                    try {
                        merges = mergeDuplicates(complexObjects);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (merges <= 0) {
                        break;
                    }
                    System.out.printf("Pass %d - Merged objects: %d\n\n", Integer.valueOf(pass), Integer.valueOf(merges));
                    pass++;
                }
                System.out.printf("Pass %d - No merged objects\n\n", Integer.valueOf(pass));
                pdDocument1.save(p);
            } catch (Throwable th) {
                if (pdDocument1 != null) {
                    pdDocument1.close();
                }
                throw th;
            }
        } catch (IOException e2) {
            e2.printStackTrace();
            if (pdDocument1 == null) {
                return;
            }
        }
        if (pdDocument1 != null) {
            pdDocument1.close();
        }
    }

    public void optimizeMyFile(PDDocument pdDocument) throws IOException {
        Map<COSBase, Collection<Reference>> complexObjects = findComplexObjects(pdDocument);
        int pass = 0;
        while (true) {
            int merges = mergeDuplicates(complexObjects);
            if (merges <= 0) {
                System.out.printf("Pass %d - No merged objects\n\n", Integer.valueOf(pass));
                return;
            } else {
                System.out.printf("Pass %d - Merged objects: %d\n\n", Integer.valueOf(pass), Integer.valueOf(merges));
                pass++;
            }
        }
    }

    Map<COSBase, Collection<Reference>> findComplexObjects(PDDocument pdDocument) {
        COSBase catalogDictionary = pdDocument.getDocumentCatalog().mo2446getCOSObject();
        Map<COSBase, Collection<Reference>> incomingReferences = new HashMap<>();
        incomingReferences.put(catalogDictionary, new ArrayList<>());
        Set<COSBase> lastPass = Collections.singleton(catalogDictionary);
        Set<COSBase> thisPass = new HashSet<>();
        while (!lastPass.isEmpty()) {
            for (COSBase object : lastPass) {
                if (object instanceof COSArray) {
                    COSArray array = (COSArray) object;
                    for (int i = 0; i < array.size(); i++) {
                        addTarget(new ArrayReference(array, i), incomingReferences, thisPass);
                    }
                } else if (object instanceof COSDictionary) {
                    COSDictionary dictionary = (COSDictionary) object;
                    for (COSName key : dictionary.keySet()) {
                        addTarget(new DictionaryReference(dictionary, key), incomingReferences, thisPass);
                    }
                }
            }
            lastPass = thisPass;
            thisPass = new HashSet<>();
        }
        return incomingReferences;
    }

    void addTarget(Reference reference, Map<COSBase, Collection<Reference>> incomingReferences, Set<COSBase> thisPass) {
        COSBase object = reference.getTo();
        if ((object instanceof COSArray) || (object instanceof COSDictionary)) {
            Collection<Reference> incoming = incomingReferences.get(object);
            if (incoming == null) {
                incoming = new ArrayList();
                incomingReferences.put(object, incoming);
                thisPass.add(object);
            }
            incoming.add(reference);
        }
    }

    int mergeDuplicates(Map<COSBase, Collection<Reference>> complexObjects) throws IOException {
        List<HashOfCOSBase> hashes = new ArrayList<>(complexObjects.size());
        for (COSBase object : complexObjects.keySet()) {
            hashes.add(new HashOfCOSBase(object));
        }
        Collections.sort(hashes);
        int removedDuplicates = 0;
        if (hashes.isEmpty()) {
            return 0;
        }
        int runStart = 0;
        int runHash = hashes.get(0).hash;
        for (int i = 1; i < hashes.size(); i++) {
            int hash = hashes.get(i).hash;
            if (hash != runHash) {
                int runSize = i - runStart;
                if (runSize != 1) {
                    System.out.printf("Equal hash %d for %d elements.\n", Integer.valueOf(runHash), Integer.valueOf(runSize));
                    removedDuplicates += mergeRun(complexObjects, hashes.subList(runStart, i));
                }
                runHash = hash;
                runStart = i;
            }
        }
        int i2 = hashes.size();
        int runSize2 = i2 - runStart;
        if (runSize2 != 1) {
            System.out.printf("Equal hash %d for %d elements.\n", Integer.valueOf(runHash), Integer.valueOf(runSize2));
            return removedDuplicates + mergeRun(complexObjects, hashes.subList(runStart, hashes.size()));
        }
        return removedDuplicates;
    }

    int mergeRun(Map<COSBase, Collection<Reference>> complexObjects, List<HashOfCOSBase> run) {
        int removedDuplicates = 0;
        List<List<COSBase>> duplicateSets = new ArrayList<>();
        for (HashOfCOSBase entry : run) {
            COSBase element = entry.object;
            Iterator<List<COSBase>> it = duplicateSets.iterator();
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                List<COSBase> duplicateSet = it.next();
                if (equals(element, duplicateSet.get(0))) {
                    duplicateSet.add(element);
                    element = null;
                    break;
                }
            }
            if (element != null) {
                List<COSBase> duplicateSet2 = new ArrayList<>();
                duplicateSet2.add(element);
                duplicateSets.add(duplicateSet2);
            }
        }
        System.out.printf("Identified %d set(s) of identical objects in run.\n", Integer.valueOf(duplicateSets.size()));
        for (List<COSBase> duplicateSet3 : duplicateSets) {
            if (duplicateSet3.size() > 1) {
                COSBase surviver = duplicateSet3.remove(0);
                Collection<Reference> surviverReferences = complexObjects.get(surviver);
                for (COSBase object : duplicateSet3) {
                    Collection<Reference> references = complexObjects.get(object);
                    for (Reference reference : references) {
                        reference.setTo(surviver);
                        surviverReferences.add(reference);
                    }
                    complexObjects.remove(object);
                    removedDuplicates++;
                }
                surviver.setDirect(false);
            }
        }
        return removedDuplicates;
    }

    boolean equals(COSBase a, COSBase b) {
        if (a instanceof COSArray) {
            if (b instanceof COSArray) {
                COSArray aArray = (COSArray) a;
                COSArray bArray = (COSArray) b;
                if (aArray.size() == bArray.size()) {
                    for (int i = 0; i < aArray.size(); i++) {
                        if (!resolve(aArray.get(i)).equals(resolve(bArray.get(i)))) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        } else if ((a instanceof COSDictionary) && (b instanceof COSDictionary)) {
            COSDictionary aDict = (COSDictionary) a;
            COSDictionary bDict = (COSDictionary) b;
            Set<COSName> keys = aDict.keySet();
            if (keys.size() == bDict.keySet().size()) {
                for (COSName key : keys) {
                    if (!resolve(aDict.getItem(key)).equals(bDict.getItem(key))) {
                        return false;
                    }
                }
                return true;
            }
        }
        return false;
    }

    static COSBase resolve(COSBase object) {
        while (object instanceof COSObject) {
            object = ((COSObject) object).getObject();
        }
        return object;
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes9.dex */
    public static class ArrayReference implements Reference {
        final COSArray from;
        final int index;

        public ArrayReference(COSArray array, int index) {
            this.from = array;
            this.index = index;
        }

        @Override // com.pdf.reader.deed.managepdf.OptimisePdf.Reference
        public COSBase getFrom() {
            return this.from;
        }

        @Override // com.pdf.reader.deed.managepdf.OptimisePdf.Reference
        public COSBase getTo() {
            return OptimisePdf.resolve(this.from.get(this.index));
        }

        @Override // com.pdf.reader.deed.managepdf.OptimisePdf.Reference
        public void setTo(COSBase to) {
            this.from.set(this.index, to);
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes9.dex */
    public static class DictionaryReference implements Reference {
        final COSDictionary from;
        final COSName key;

        public DictionaryReference(COSDictionary dictionary, COSName key) {
            this.from = dictionary;
            this.key = key;
        }

        @Override // com.pdf.reader.deed.managepdf.OptimisePdf.Reference
        public COSBase getFrom() {
            return this.from;
        }

        @Override // com.pdf.reader.deed.managepdf.OptimisePdf.Reference
        public COSBase getTo() {
            return OptimisePdf.resolve(this.from.getDictionaryObject(this.key));
        }

        @Override // com.pdf.reader.deed.managepdf.OptimisePdf.Reference
        public void setTo(COSBase to) {
            this.from.setItem(this.key, to);
        }
    }

    /* JADX INFO: Access modifiers changed from: package-private */
    /* loaded from: classes9.dex */
    public static class HashOfCOSBase implements Comparable<HashOfCOSBase> {
        final int hash;
        final COSBase object;

        public HashOfCOSBase(COSBase object) throws IOException {
            this.object = object;
            this.hash = calculateHash(object);
        }

        int calculateHash(COSBase object) throws IOException {
            if (object instanceof COSArray) {
                int result = 1;
                Iterator<COSBase> it = ((COSArray) object).iterator();
                while (it.hasNext()) {
                    COSBase member = it.next();
                    result = (result * 31) + member.hashCode();
                }
                return result;
            } else if (object instanceof COSDictionary) {
                int result2 = 3;
                for (Map.Entry<COSName, COSBase> entry : ((COSDictionary) object).entrySet()) {
                    result2 += entry.hashCode();
                }
                if (object instanceof COSStream) {
                    try {
                        InputStream data = ((COSStream) object).createRawInputStream();
                        MessageDigest md = MessageDigest.getInstance("MD5");
                        byte[] buffer = new byte[8192];
                        while (true) {
                            int bytesRead = data.read(buffer);
                            if (bytesRead < 0) {
                                break;
                            }
                            md.update(buffer, 0, bytesRead);
                        }
                        result2 = (result2 * 31) + Arrays.hashCode(md.digest());
                        if (data != null) {
                            data.close();
                        }
                    } catch (NoSuchAlgorithmException e) {
                        throw new IOException(e);
                    }
                }
                return result2;
            } else {
                throw new IllegalArgumentException(String.format("Unknown complex COSBase type %s", object.getClass().getName()));
            }
        }

        @Override // java.lang.Comparable
        public int compareTo(HashOfCOSBase o) {
            int result = Integer.compare(this.hash, o.hash);
            if (result == 0) {
                return Integer.compare(hashCode(), o.hashCode());
            }
            return result;
        }
    }
}