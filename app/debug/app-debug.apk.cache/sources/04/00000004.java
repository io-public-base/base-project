package com.base.architecture.io.baseclasses;

import android.app.Dialog;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProviders;
import com.base.architecture.io.baseclasses.BaseViewModel;
import com.base.architecture.io.eventbus.EventBus;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;
import org.apache.pdfbox.pdmodel.interactive.measurement.PDNumberFormatDictionary;

/* compiled from: BaseActivity.kt */
@Metadata(d1 = {"\u0000H\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\r\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\b&\u0018\u0000*\b\b\u0000\u0010\u0001*\u00020\u0002*\b\b\u0001\u0010\u0003*\u00020\u00042\u00020\u0005B\u0005¢\u0006\u0002\u0010\u0006J\b\u0010%\u001a\u00020&H\u0002J\b\u0010'\u001a\u00020&H\u0002J\u0012\u0010(\u001a\u00020&2\b\u0010)\u001a\u0004\u0018\u00010*H\u0014J\b\u0010+\u001a\u00020&H\u0014J\b\u0010,\u001a\u00020&H\u0014J\b\u0010-\u001a\u00020&H\u0014J\b\u0010.\u001a\u00020&H\u0016J\b\u0010/\u001a\u00020&H\u0016R\u0012\u0010\u0007\u001a\u00020\bX¦\u0004¢\u0006\u0006\u001a\u0004\b\t\u0010\nR\u0014\u0010\u000b\u001a\u00020\b8gX¦\u0004¢\u0006\u0006\u001a\u0004\b\f\u0010\nR\u001a\u0010\r\u001a\u00020\u000eX\u0084.¢\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u0010\u0010\u0013\u001a\u00028\u0000X\u0082.¢\u0006\u0004\n\u0002\u0010\u0014R\u001c\u0010\u0015\u001a\u00028\u0001X\u0084.¢\u0006\u0010\n\u0002\u0010\u001a\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u001a\u0010\u001b\u001a\u00020\u001cX\u0086.¢\u0006\u000e\n\u0000\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R\u0018\u0010!\u001a\b\u0012\u0004\u0012\u00028\u00010\"X¦\u0004¢\u0006\u0006\u001a\u0004\b#\u0010$¨\u00060"}, d2 = {"Lcom/pdf/reader/deed/baseclasses/BaseActivity;", PDNumberFormatDictionary.FRACTIONAL_DISPLAY_TRUNCATE, "Landroidx/databinding/ViewDataBinding;", "V", "Lcom/pdf/reader/deed/baseclasses/BaseViewModel;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "bindingVariable", "", "getBindingVariable", "()I", "layoutId", "getLayoutId", "loadingDialog", "Landroid/app/Dialog;", "getLoadingDialog", "()Landroid/app/Dialog;", "setLoadingDialog", "(Landroid/app/Dialog;)V", "mViewDataBinding", "Landroidx/databinding/ViewDataBinding;", "mViewModel", "getMViewModel", "()Lcom/pdf/reader/deed/baseclasses/BaseViewModel;", "setMViewModel", "(Lcom/pdf/reader/deed/baseclasses/BaseViewModel;)V", "Lcom/pdf/reader/deed/baseclasses/BaseViewModel;", "pdfEventBus", "Lcom/pdf/reader/deed/eventbus/PdfEventBus;", "getPdfEventBus", "()Lcom/pdf/reader/deed/eventbus/PdfEventBus;", "setPdfEventBus", "(Lcom/pdf/reader/deed/eventbus/PdfEventBus;)V", "viewModel", "Ljava/lang/Class;", "getViewModel", "()Ljava/lang/Class;", "databindingWithViewModel", "", "initEventBus", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onDestroy", "onStart", "onStop", "subscribeToNetworkLiveData", "subscribeToViewLiveData", "app_debug"}, k = 1, mv = {1, 5, 1}, xi = 48)
/* loaded from: classes14.dex */
public abstract class BaseActivity<T extends ViewDataBinding, V extends BaseViewModel> extends AppCompatActivity {
    protected Dialog loadingDialog;
    private T mViewDataBinding;
    protected V mViewModel;
    public PdfEventBus pdfEventBus;

    public void _$_clearFindViewByIdCache() {
    }

    public abstract int getBindingVariable();

    public abstract int getLayoutId();

    public abstract Class<V> getViewModel();

    protected final V getMViewModel() {
        V v = this.mViewModel;
        if (v != null) {
            return v;
        }
        Intrinsics.throwUninitializedPropertyAccessException("mViewModel");
        return null;
    }

    protected final void setMViewModel(V v) {
        Intrinsics.checkNotNullParameter(v, "<set-?>");
        this.mViewModel = v;
    }

    protected final Dialog getLoadingDialog() {
        Dialog dialog = this.loadingDialog;
        if (dialog != null) {
            return dialog;
        }
        Intrinsics.throwUninitializedPropertyAccessException("loadingDialog");
        return null;
    }

    protected final void setLoadingDialog(Dialog dialog) {
        Intrinsics.checkNotNullParameter(dialog, "<set-?>");
        this.loadingDialog = dialog;
    }

    public final PdfEventBus getPdfEventBus() {
        PdfEventBus pdfEventBus = this.pdfEventBus;
        if (pdfEventBus != null) {
            return pdfEventBus;
        }
        Intrinsics.throwUninitializedPropertyAccessException("pdfEventBus");
        return null;
    }

    public final void setPdfEventBus(PdfEventBus pdfEventBus) {
        Intrinsics.checkNotNullParameter(pdfEventBus, "<set-?>");
        this.pdfEventBus = pdfEventBus;
    }

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // androidx.fragment.app.FragmentActivity, androidx.activity.ComponentActivity, androidx.core.app.ComponentActivity, android.app.Activity
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        databindingWithViewModel();
        subscribeToNetworkLiveData();
        subscribeToViewLiveData();
        initEventBus();
    }

    /* JADX WARN: Multi-variable type inference failed */
    private final void databindingWithViewModel() {
        try {
            T t = (T) DataBindingUtil.setContentView(this, getLayoutId());
            Intrinsics.checkNotNullExpressionValue(t, "setContentView(this, layoutId)");
            this.mViewDataBinding = t;
            ViewModel viewModel = ViewModelProviders.of(this).get(getViewModel());
            Intrinsics.checkNotNullExpressionValue(viewModel, "of(this).get(viewModel)");
            setMViewModel((BaseViewModel) viewModel);
            T t2 = this.mViewDataBinding;
            T t3 = null;
            if (t2 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("mViewDataBinding");
                t2 = null;
            }
            t2.setVariable(getBindingVariable(), getMViewModel());
            T t4 = this.mViewDataBinding;
            if (t4 == null) {
                Intrinsics.throwUninitializedPropertyAccessException("mViewDataBinding");
            } else {
                t3 = t4;
            }
            t3.executePendingBindings();
        } catch (Exception e) {
        }
    }

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStart() {
        super.onStart();
        initEventBus();
    }

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onStop() {
        super.onStop();
        getPdfEventBus().unsubscribe(this);
    }

    /* JADX INFO: Access modifiers changed from: protected */
    @Override // androidx.appcompat.app.AppCompatActivity, androidx.fragment.app.FragmentActivity, android.app.Activity
    public void onDestroy() {
        super.onDestroy();
        getPdfEventBus().unsubscribe(this);
    }

    public void subscribeToNetworkLiveData() {
    }

    public void subscribeToViewLiveData() {
    }

    private final void initEventBus() {
        setPdfEventBus(new PdfEventBus());
        getPdfEventBus().subscribe(this);
    }
}