package com.base.architecture.io.managepdf;

import android.webkit.WebView;
import kotlin.Metadata;

/* compiled from: AnnotatePdf.kt */
@Metadata(d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000e\n\u0002\b\u0007\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u0007\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\b\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\t\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\n\u001a\u00020\u000b2\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\f\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\r\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u000e\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u000f\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u0010\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006J\u0010\u0010\u0011\u001a\u00020\u00042\b\u0010\u0005\u001a\u0004\u0018\u00010\u0006¨\u0006\u0012"}, d2 = {"Lcom/pdf/reader/deed/managepdf/AnnotatePdf;", "", "()V", "changeTextColor", "", "customPdfView", "Landroid/webkit/WebView;", "disableEditing", "drawUnderline", "enablePdfEditing", "getSelection", "", "highlightPdfContent", "makeFontsBold", "removeEditingBorders", "scalePdfView", "selfInvokingFun", "textStyle", "app_debug"}, k = 1, mv = {1, 5, 1}, xi = 48)
/* loaded from: classes9.dex */
public final class AnnotatePdf {
    public static final AnnotatePdf INSTANCE = new AnnotatePdf();

    private AnnotatePdf() {
    }

    public final void enablePdfEditing(WebView customPdfView) {
        if (customPdfView == null) {
            return;
        }
        customPdfView.evaluateJavascript(AnnotationConstants.CMD_ENABLE_EDITING, $$Lambda$AnnotatePdf$mIBlVGKnwLGBRV6O9LuNZXZ2q5U.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: enablePdfEditing$lambda-0  reason: not valid java name */
    public static final void m584enablePdfEditing$lambda0(String value) {
    }

    public final void removeEditingBorders(WebView customPdfView) {
        if (customPdfView == null) {
            return;
        }
        customPdfView.evaluateJavascript(AnnotationConstants.DISABLE_EDITING_BORDERS, $$Lambda$AnnotatePdf$X0NTIkchkocAidE_YQJMw7Q2r88.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: removeEditingBorders$lambda-1  reason: not valid java name */
    public static final void m589removeEditingBorders$lambda1(String value) {
    }

    public final void selfInvokingFun(WebView customPdfView) {
        if (customPdfView == null) {
            return;
        }
        customPdfView.evaluateJavascript(AnnotationConstants.SELF_INVOKING_FUN, $$Lambda$AnnotatePdf$jvjNdW_NZyil1Am8fi8ZF29SRh8.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: selfInvokingFun$lambda-2  reason: not valid java name */
    public static final void m591selfInvokingFun$lambda2(String value) {
    }

    public final void highlightPdfContent(WebView customPdfView) {
        if (customPdfView == null) {
            return;
        }
        customPdfView.evaluateJavascript(AnnotationConstants.CMD_HIGHLIGHTER, $$Lambda$AnnotatePdf$LF1AJJu5JYvVuKLUJky2sQ2cetI.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: highlightPdfContent$lambda-3  reason: not valid java name */
    public static final void m586highlightPdfContent$lambda3(String value) {
    }

    public final void changeTextColor(WebView customPdfView) {
        if (customPdfView == null) {
            return;
        }
        customPdfView.evaluateJavascript(AnnotationConstants.CMD_CHANGE_COL, $$Lambda$AnnotatePdf$7rdeTDLE_4HhFI6xvCQonIV8PZA.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: changeTextColor$lambda-4  reason: not valid java name */
    public static final void m581changeTextColor$lambda4(String value) {
    }

    public final void makeFontsBold(WebView customPdfView) {
        if (customPdfView == null) {
            return;
        }
        customPdfView.evaluateJavascript(AnnotationConstants.CMD_ST_BOLD, $$Lambda$AnnotatePdf$xGqh_00aAqfaRYVtfG1bBGvWxzo.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: makeFontsBold$lambda-5  reason: not valid java name */
    public static final void m588makeFontsBold$lambda5(String value) {
    }

    public final void drawUnderline(WebView customPdfView) {
        if (customPdfView == null) {
            return;
        }
        customPdfView.evaluateJavascript(AnnotationConstants.CMD_UNDERLINE_TX, $$Lambda$AnnotatePdf$yc4Y158CVdWUs1B9vmhvV4A0EI.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: drawUnderline$lambda-6  reason: not valid java name */
    public static final void m583drawUnderline$lambda6(String value) {
    }

    public final void textStyle(WebView customPdfView) {
        if (customPdfView == null) {
            return;
        }
        customPdfView.evaluateJavascript(AnnotationConstants.CMD_TX_ITALIC, $$Lambda$AnnotatePdf$ntPghBbyLySHdnQDwndl2n23Gks.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: textStyle$lambda-7  reason: not valid java name */
    public static final void m592textStyle$lambda7(String value) {
    }

    public final void disableEditing(WebView customPdfView) {
        if (customPdfView == null) {
            return;
        }
        customPdfView.evaluateJavascript(AnnotationConstants.DISABLE_EDITING, $$Lambda$AnnotatePdf$q2v6Ff2RVNNaw4HBLQwrvciXxyk.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: disableEditing$lambda-8  reason: not valid java name */
    public static final void m582disableEditing$lambda8(String value) {
    }

    public final void scalePdfView(WebView customPdfView) {
        if (customPdfView == null) {
            return;
        }
        customPdfView.evaluateJavascript(AnnotationConstants.SCALE_PDF_VIEW, $$Lambda$AnnotatePdf$bqvR_1haDdI4C_H8P5PNBXyr4wk.INSTANCE);
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: scalePdfView$lambda-9  reason: not valid java name */
    public static final void m590scalePdfView$lambda9(String value) {
    }

    public final String getSelection(WebView customPdfView) {
        if (customPdfView == null) {
            return "";
        }
        customPdfView.evaluateJavascript("(function(){return window.getSelection().toString()})()", $$Lambda$AnnotatePdf$oMewlabjdoE3P8i0MyXt50IvbVU.INSTANCE);
        return "";
    }

    /* JADX INFO: Access modifiers changed from: private */
    /* renamed from: getSelection$lambda-10  reason: not valid java name */
    public static final void m585getSelection$lambda10(String v) {
    }
}