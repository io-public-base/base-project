package com.base.architecture.io.managepdf;

import android.content.Context;
import com.viliussutkus89.android.pdf2htmlex.pdf2htmlEX;
import java.io.File;
import kotlin.Metadata;
import kotlin.jvm.internal.Intrinsics;

/* compiled from: PdfRendring.kt */
@Metadata(d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\bÆ\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002¢\u0006\u0002\u0010\u0002J\u0018\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u0004J \u0010\b\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u00062\u0006\u0010\u0007\u001a\u00020\u00042\u0006\u0010\t\u001a\u00020\n¨\u0006\u000b"}, d2 = {"Lcom/pdf/reader/deed/managepdf/PdfRendring;", "", "()V", "render", "Ljava/io/File;", "context", "Landroid/content/Context;", "inputFile", "renderWithPassword", "password", "", "app_debug"}, k = 1, mv = {1, 5, 1}, xi = 48)
/* loaded from: classes9.dex */
public final class PdfRendring {
    public static final PdfRendring INSTANCE = new PdfRendring();

    private PdfRendring() {
    }

    public final File render(Context context, File inputFile) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(inputFile, "inputFile");
        File it = new pdf2htmlEX(context).setInputPDF(inputFile).convert();
        return it;
    }

    public final File renderWithPassword(Context context, File inputFile, String password) {
        Intrinsics.checkNotNullParameter(context, "context");
        Intrinsics.checkNotNullParameter(inputFile, "inputFile");
        Intrinsics.checkNotNullParameter(password, "password");
        File it = new pdf2htmlEX(context).setInputPDF(inputFile).setUserPassword(password).setOwnerPassword(password).convert();
        return it;
    }
}